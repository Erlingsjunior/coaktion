export const setUserCookie = () => {
  const now = new Date()
  const time = now.getTime()
  const expireTime = time + 5 * 1000 * 60
  now.setTime(expireTime)
  document.cookie = `userToken=logado; expires=${now.toUTCString()}; path=/`
}
