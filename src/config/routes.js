import React from 'react'
import { BrowserRouter, Route, Switch, Redirect } from 'react-router-dom'

import ScrollTop from '../hooks/ScrollTop'

import Home from '../pages/Home'
import BookDetails from '../pages/BookDetails'
import Login from '../pages/Login'
import List from '../pages/Admin/List'
import Edit from '../pages/Admin/Edit'
import Insert from '../pages/Admin/Insert'
import BookDetailsAdmin from '../pages/Admin/BookDetailsAdmin'

import { isAuthenticated } from './auth'

const PrivateRoute = ({ component: Component, ...rest }) => (
  <Route
    {...rest}
    render={(props) =>
      isAuthenticated() ? <Component {...props} /> : <Redirect to='/login' />
    }
  />
)

const Routes = () => (
  <BrowserRouter>
    <ScrollTop />
    <Switch>
      <Route exact path='/' component={Home} />
      <Route path='/detalhes/:bookId' component={BookDetails} />
      <Route path='/login' component={Login} />

      <PrivateRoute exact path='/admin/listagem' component={List} />
      <PrivateRoute exact path='/admin/editar/:bookId' component={Edit} />
      <PrivateRoute
        exact
        path='/admin/detalhes/:bookId'
        component={BookDetailsAdmin}
      />
      <PrivateRoute exact path='/admin/inserir' component={Insert} />

      <Redirect from='*' to='/' />
    </Switch>
  </BrowserRouter>
)

export default Routes
