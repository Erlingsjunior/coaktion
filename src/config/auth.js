export const isAuthenticated = () => {
  const userToken = getCookie('userToken')
  if (userToken) {
    return true
  }
  return false
}

const getCookie = (name) => {
  const match = document.cookie.match(new RegExp('(^| )' + name + '=([^;]+)'))
  if (match) {
    return match[2]
  } else {
    return ''
  }
}
