import React from 'react'

import { ThemeProvider } from '@material-ui/core/styles'
import { ThemeProvider as StyledThemeProvider } from 'styled-components'

import Theme from './styles/theme'
import ThemeMUI from './styles/themeMUI'
import GlobalStyle from './styles/global'

import Routes from './config/routes'

function App() {
  return (
    <ThemeProvider theme={ThemeMUI}>
      <StyledThemeProvider theme={Theme}>
        <GlobalStyle />

        <Routes />
      </StyledThemeProvider>
    </ThemeProvider>
  )
}

export default App
