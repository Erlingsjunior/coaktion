import { useState, useEffect } from 'react'

const useFetchData = (initialReqBody, initialData = []) => {
  const [data, setData] = useState(initialData)
  const [reqBody, setReqBody] = useState(initialReqBody)
  const [isLoading, setIsLoading] = useState(false)
  const [isError, setIsError] = useState(false)

  useEffect(() => {
    const fetchData = async () => {
      if (reqBody) {
        setIsError(false)
        setIsLoading(true)

        try {
          const response = await fetch(reqBody.url, reqBody.config)
          if (!response.ok) throw new Error(response.statusText)

          const responseData = await response.json()

          setData(responseData)
        } catch (error) {
          console.error(error)
          setIsError(true)
        }

        setIsLoading(false)
      }
    }

    fetchData()
  }, [reqBody])

  return [{ data, isLoading, isError }, setReqBody]
}

export default useFetchData
