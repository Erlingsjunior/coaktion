import { useEffect } from "react";
import { withRouter } from "react-router-dom";

// garante que todas as páginas serão inicializadas com a rolagem no topo
function ScrollTop({ history }) {
  useEffect(() => {
    const unlisten = history.listen(() => {
      window.scrollTo(0, 0);
    });
    return () => {
      unlisten();
    };
  }, [history]);

  return null;
}

export default withRouter(ScrollTop);
