import { Link } from 'react-router-dom'

import { Container, Title, Content, InfoBook } from '../../components'
import * as S from './styled'

import useFetchData from '../../hooks/useFetchData'

export default function BookDetails(props) {
  const bookId = props.match.params.bookId
  const [{ data, isLoading, isError }] = useFetchData({
    url: `https://coaktion.herokuapp.com/books/${bookId}`,
    config: {}
  })

  return (
    <S.Wrapper>
      <Container>
        <Content>
          <Link to='/'>Voltar</Link>

          <Title>Detalhes</Title>

          {isError && <p>Aconteceu algum erro...</p>}

          {isLoading ? (
            <p>Carregando...</p>
          ) : (
            <>
              {!isError && Object.keys(data).length > 0 && (
                <>
                  <InfoBook>
                    <img src={data.cover_image || ''} alt={data.title} />

                    <div className='features-box'>
                      <div className='features -title'>
                        <span>Título</span>
                        <h2>{data.title}</h2>
                      </div>

                      <div className='features -author'>
                        <span>Autor</span>
                        <h2>{data.author_name}</h2>
                      </div>

                      <div className='features -pages'>
                        <span>Nº páginas</span>
                        <h2>{data.pages}</h2>
                      </div>

                      <div className='features published'>
                        <span>Ano de publicação</span>
                        <h2>{data.releaseDate}</h2>
                      </div>
                    </div>
                  </InfoBook>
                </>
              )}
            </>
          )}
        </Content>
      </Container>
    </S.Wrapper>
  )
}
