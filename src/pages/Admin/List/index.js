import { useState } from 'react'
import { Link } from 'react-router-dom'

import DeleteIcon from '@material-ui/icons/Delete'
import EditIcon from '@material-ui/icons/Edit'
import VisibilityIcon from '@material-ui/icons/Visibility'

import {
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Paper,
  IconButton,
  Button,
  Snackbar
} from '@material-ui/core'

import Alert from '@material-ui/lab/Alert'

import { Container, Title, Content } from '../../../components'
import * as S from './styled'

import useFetchData from '../../../hooks/useFetchData'

export default function List() {
  const [{ data, isLoading, isError }, setReqBody] = useFetchData({
    url: `https://coaktion.herokuapp.com/books`,
    config: {}
  })
  const [openAlert, setOpenAlert] = useState(false)

  const handleOnClickDelete = async (bookId) => {
    const reqBody = {
      url: `https://coaktion.herokuapp.com/books/${bookId}`,
      config: {
        method: 'DELETE',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json'
        }
      }
    }

    await deleteBook(reqBody)

    setOpenAlert(!openAlert)
    setReqBody({
      url: `https://coaktion.herokuapp.com/books`
    })
  }

  const deleteBook = async (reqBody) => {
    const response = await fetch(reqBody.url, reqBody.config)
    const data = await response.json()
    return data
  }

  return (
    <S.Wrapper>
      <Container>
        <Content>
          <Snackbar
            open={openAlert}
            autoHideDuration={6000}
            onClose={() => setOpenAlert(!openAlert)}
          >
            <Alert onClose={() => setOpenAlert(!openAlert)} severity='success'>
              Livro excluído com sucesso!
            </Alert>
          </Snackbar>

          <Title>Listagem</Title>

          <Link to='/admin/inserir'>
            <Button
              variant='contained'
              color='primary'
              style={{ marginBottom: '25px' }}
            >
              Inserir um novo livro
            </Button>
          </Link>

          {isError && <p>Aconteceu algum erro!</p>}

          {isLoading ? (
            <p>Carregando...</p>
          ) : (
            <>
              {!isError && data.length > 0 && (
                <>
                  <TableContainer component={Paper}>
                    <Table aria-label='simple table'>
                      <TableHead>
                        <TableRow>
                          <TableCell>Capa</TableCell>
                          <TableCell>Título</TableCell>
                          <TableCell>Autor</TableCell>
                          <TableCell>Páginas</TableCell>
                          <TableCell>Publicado em</TableCell>
                          <TableCell align='center'>Ações</TableCell>
                        </TableRow>
                      </TableHead>
                      <TableBody>
                        {data.map((row) => (
                          <TableRow key={row._id}>
                            <TableCell component='th' scope='row'>
                              <img
                                src={row.cover_image}
                                height='100px'
                                alt={row.cover_image}
                              />
                            </TableCell>
                            <TableCell>{row.title}</TableCell>
                            <TableCell>{row.author_name}</TableCell>
                            <TableCell>{row.pages}</TableCell>
                            <TableCell>{row.releaseDate}</TableCell>
                            <TableCell align='center'>
                              <IconButton
                                aria-label='delete'
                                onClick={() => handleOnClickDelete(row._id)}
                              >
                                <DeleteIcon
                                  fontSize='large'
                                  color='secondary'
                                />
                              </IconButton>
                              <Link to={`/admin/editar/${row._id}`}>
                                <IconButton aria-label='editar'>
                                  <EditIcon fontSize='large' color='primary' />
                                </IconButton>
                              </Link>

                              <Link to={`/admin/detalhes/${row._id}`}>
                                <IconButton aria-label='visualizar'>
                                  <VisibilityIcon
                                    fontSize='large'
                                    color='primary'
                                  />
                                </IconButton>
                              </Link>
                            </TableCell>
                          </TableRow>
                        ))}
                      </TableBody>
                    </Table>
                  </TableContainer>
                </>
              )}
            </>
          )}
        </Content>
      </Container>
    </S.Wrapper>
  )
}
