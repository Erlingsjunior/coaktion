import { useState } from 'react'
import { Link } from 'react-router-dom'

import { TextField, Button, Snackbar } from '@material-ui/core'
import Alert from '@material-ui/lab/Alert'

import { Container, Title, Form, Content } from '../../../components'
import * as S from './styled'

import useFetchData from '../../../hooks/useFetchData'

export default function Insert() {
  const [openAlert, setOpenAlert] = useState(false)
  const [, setReqBody] = useFetchData()

  const clearState = () => ({
    author_name: '',
    title: '',
    cover_image: '',
    pages: '',
    releaseDate: '',
    quantity: ''
  })

  const [textFields, setTextFields] = useState(clearState)

  const handleOnChange = (e) => {
    const type = e.target.parentElement.parentElement.dataset['type']
    const value = e.target.value

    let newState = { ...textFields }
    newState[type] = value

    setTextFields(newState)
  }

  const handleSubmit = (e) => {
    e.preventDefault()

    const reqBody = {
      url: 'https://coaktion.herokuapp.com/books',
      config: {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(textFields)
      }
    }

    setReqBody(reqBody)

    setOpenAlert(!openAlert)
    setTextFields(clearState)
  }

  return (
    <S.Wrapper>
      <Container>
        <Content>
          <Snackbar
            open={openAlert}
            autoHideDuration={6000}
            onClose={() => setOpenAlert(!openAlert)}
          >
            <Alert onClose={() => setOpenAlert(!openAlert)} severity='success'>
              Livro inserido com sucesso!
            </Alert>
          </Snackbar>

          <Link to='/admin/listagem'>Voltar</Link>
          <Title>Inserir</Title>

          <Form onSubmit={handleSubmit}>
            <TextField
              label='Título'
              data-type='title'
              value={textFields.title}
              onChange={handleOnChange}
            />
            <TextField
              label='Autor'
              data-type='author_name'
              value={textFields.author_name}
              onChange={handleOnChange}
            />
            <TextField
              label='Imagem(Url)'
              data-type='cover_image'
              value={textFields.cover_image}
              onChange={handleOnChange}
            />
            <TextField
              label='Nº de páginas'
              data-type='pages'
              value={textFields.pages}
              onChange={handleOnChange}
            />
            <TextField
              label='Ano de publicação'
              data-type='releaseDate'
              value={textFields.releaseDate}
              onChange={handleOnChange}
            />
            <TextField
              label='Quantidade'
              data-type='quantity'
              value={textFields.quantity}
              onChange={handleOnChange}
            />

            <Button
              variant='contained'
              disabled={
                !textFields.author_name ||
                !textFields.title ||
                !textFields.cover_image ||
                !textFields.pages ||
                !textFields.releaseDate ||
                !textFields.quantity
              }
              color='primary'
              type='submit'
            >
              Inserir
            </Button>
          </Form>
        </Content>
      </Container>
    </S.Wrapper>
  )
}
