import { useEffect, useState } from 'react'
import { Link } from 'react-router-dom'

import { TextField, Button, Snackbar } from '@material-ui/core'
import Alert from '@material-ui/lab/Alert'

import { Container, Title, Form, Content } from '../../../components'
import * as S from './styled'

import useFetchData from '../../../hooks/useFetchData'

export default function BookDetailsAdmin(props) {
  const bookId = props.match.params.bookId
  const [{ data, isLoading, isError }] = useFetchData({
    url: `https://coaktion.herokuapp.com/books/${bookId}`,
    config: {}
  })
  const [openAlert, setOpenAlert] = useState(false)
  const [textFields, setTextFields] = useState(data)

  const handleOnChange = (e) => {
    const type = e.target.parentElement.parentElement.dataset['type']
    const value = e.target.value

    let newState = { ...textFields }
    newState[type] = value

    setTextFields(newState)
  }

  const handleSubmit = async (e) => {
    e.preventDefault()

    const reqBody = {
      url: `https://coaktion.herokuapp.com/books/${bookId}`,
      config: {
        method: 'PUT',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(textFields)
      }
    }

    await putBook(reqBody)
    setOpenAlert(!openAlert)
  }

  const putBook = async (reqBody) => {
    const response = await fetch(reqBody.url, reqBody.config)
    const data = await response.json()
    return data
  }

  useEffect(() => {
    setTextFields(data)
  }, [data])

  return (
    <S.Wrapper>
      <Container>
        <Content>
          <Link to='/admin/listagem'>Voltar</Link>
          <Title>Editar</Title>

          {isError && <p>Aconteceu algum erro...</p>}

          {isLoading ? (
            <p>Carregando...</p>
          ) : (
            <>
              {!isError && Object.keys(data).length > 0 && (
                <>
                  <Snackbar
                    open={openAlert}
                    autoHideDuration={6000}
                    onClose={() => setOpenAlert(!openAlert)}
                  >
                    <Alert
                      onClose={() => setOpenAlert(!openAlert)}
                      severity='success'
                    >
                      Livro editado com sucesso!
                    </Alert>
                  </Snackbar>

                  <Form onSubmit={handleSubmit}>
                    <TextField
                      label='Título'
                      data-type='title'
                      value={textFields.title}
                      onChange={handleOnChange}
                    />
                    <TextField
                      label='Autor'
                      data-type='author_name'
                      value={textFields.author_name}
                      onChange={handleOnChange}
                    />
                    <TextField
                      label='Imagem(Url)'
                      data-type='cover_image'
                      value={textFields.cover_image}
                      onChange={handleOnChange}
                    />
                    <TextField
                      label='Nº de páginas'
                      data-type='pages'
                      value={textFields.pages}
                      onChange={handleOnChange}
                    />
                    <TextField
                      label='Ano de publicação'
                      data-type='releaseDate'
                      value={textFields.releaseDate}
                      onChange={handleOnChange}
                    />
                    <TextField
                      label='Quantidade'
                      data-type='quantity'
                      value={textFields.quantity}
                      onChange={handleOnChange}
                    />

                    <Button
                      variant='contained'
                      disabled={
                        !textFields.author_name ||
                        !textFields.title ||
                        !textFields.cover_image ||
                        !textFields.pages ||
                        !textFields.releaseDate ||
                        !textFields.quantity
                      }
                      color='primary'
                      type='submit'
                    >
                      Salvar
                    </Button>
                  </Form>
                </>
              )}
            </>
          )}
        </Content>
      </Container>
    </S.Wrapper>
  )
}
