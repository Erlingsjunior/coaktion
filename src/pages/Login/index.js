import { useState, useEffect } from 'react'
import { Redirect } from 'react-router-dom'

import TextField from '@material-ui/core/TextField'
import Button from '@material-ui/core/Button'

import { isAuthenticated } from '../../config/auth'
import { setUserCookie } from '../../utils/setUserCookie'

import * as S from './styled'

export default function Login() {
  const [isError, setIsError] = useState(false)
  const [isValid, setIsValid] = useState(false)
  const [textFields, setTextFields] = useState({
    email: '',
    password: ''
  })

  const handleOnChange = (e) => {
    setIsError(false)
    setIsValid(false)
    const type = e.target.type
    const value = e.target.value

    let newState = { ...textFields }
    newState[type] = value

    setTextFields(newState)
  }

  const handleOnSubmit = (e) => {
    e.preventDefault()

    if (
      textFields.email === 'admin@admin.com' &&
      textFields.password === 'admin'
    ) {
      setUserCookie()
      setIsValid(true)
    } else {
      setIsError(true)
    }
  }

  useEffect(() => {}, [isValid])

  return isAuthenticated() ? (
    <Redirect to='/admin/listagem' />
  ) : (
    <S.Wrapper>
      <S.FormBox>
        <h2>Login</h2>
        <form noValidate autoComplete='off' onSubmit={handleOnSubmit}>
          {isError && <p style={{ color: 'red' }}>Erro! Login inválido!</p>}
          <TextField
            label='Email'
            variant='outlined'
            type='email'
            value={textFields.email}
            onChange={handleOnChange}
          />
          <TextField
            label='Senha'
            type='password'
            variant='outlined'
            value={textFields.password}
            onChange={handleOnChange}
          />
          <Button
            variant='contained'
            disabled={!textFields.email || !textFields.password}
            color='primary'
            type='submit'
          >
            Entrar
          </Button>
        </form>
      </S.FormBox>
    </S.Wrapper>
  )
}
