import styled from 'styled-components/macro'

export const Wrapper = styled.div`
  background: ${({ theme }) => theme.colors.blue};
  display: flex;
  align-items: center;
  justify-content: center;
  height: 100vh;
`

export const FormBox = styled.div`
  border-radius: 9px;
  background: #fff;
  padding: 25px;

  h2 {
    margin-bottom: 20px;
    text-align: center;
  }

  form {
    display: flex;
    flex-direction: column;
    gap: 17px;
  }
`
