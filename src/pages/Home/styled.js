import styled from 'styled-components'

export const Wrapper = styled.div``

export const Content = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr 1fr;
  gap: 25px;
  padding: 0 0 25px 0;
`
