import { Link } from 'react-router-dom'

import Typography from '@material-ui/core/Typography'

import { Container, Card, Title } from '../../components'
import * as S from './styled'

import useFetchData from '../../hooks/useFetchData'

export default function Home() {
  const [{ data, isLoading, isError }] = useFetchData({
    url: `https://coaktion.herokuapp.com/books`,
    config: {}
  })

  return (
    <S.Wrapper>
      <Container>
        <Title>Livros</Title>
        <S.Content>
          {isError && <p>Aconteceu algum erro!</p>}

          {isLoading ? (
            <p>Carregando...</p>
          ) : (
            <>
              {!isError &&
                data.length > 0 &&
                data.map((book, idx) => (
                  <Link key={idx} to={`/detalhes/${book._id}`}>
                    <Card.Wrapper>
                      <Card.ActionArea>
                        <Card.Media
                          image={
                            book.cover_image ||
                            'https://material-ui.com/static/images/cards/contemplative-reptile.jpg'
                          }
                          title={book.title}
                        />
                        <Card.Content>
                          <Typography gutterBottom variant='h5' component='h2'>
                            {book.title}
                          </Typography>
                          <Typography
                            variant='body2'
                            color='textSecondary'
                            component='p'
                          >
                            {book.author_name} - {book.pages} páginas
                          </Typography>
                        </Card.Content>
                      </Card.ActionArea>
                    </Card.Wrapper>
                  </Link>
                ))}
            </>
          )}
        </S.Content>
      </Container>
    </S.Wrapper>
  )
}
