import styled from 'styled-components/macro'

const Form = styled.form`
  display: flex;
  flex-direction: column;
  width: 300px;
  gap: 35px;
`

export default Form
