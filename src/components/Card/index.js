import styled from 'styled-components/macro'
import CardMUI from '@material-ui/core/Card'
import CardActionAreaMUI from '@material-ui/core/CardActionArea'
import CardActionsMUI from '@material-ui/core/CardActions'
import CardContentMUI from '@material-ui/core/CardContent'
import CardMediaMUI from '@material-ui/core/CardMedia'

export const Wrapper = styled(CardMUI)``

export const Media = styled(CardMediaMUI)`
  height: 240px;
`

export const ActionArea = styled(CardActionAreaMUI)``

export const Actions = styled(CardActionsMUI)``

export const Content = styled(CardContentMUI)``
