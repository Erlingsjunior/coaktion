import styled from 'styled-components/macro'

const InfoBook = styled.div`
  display: flex;
  gap: 24px;
  align-items: center;
  margin: 25px 0;

  .features {
    margin-bottom: 25px;
    span {
    }

    h2 {
    }
  }
`

export default InfoBook
