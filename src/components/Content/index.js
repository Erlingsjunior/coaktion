import styled from 'styled-components'

const Content = styled.div`
  padding: 25px;
  img {
    height: 340px;
  }
`

export default Content
