import styled from 'styled-components/macro'

const Title = styled.h1`
  margin-bottom: 25px;
`

export default Title
