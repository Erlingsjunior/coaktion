import Container from './Container'
import * as Card from './Card'
import Title from './Title'
import Form from './Form'
import Content from './Content'
import InfoBook from './InfoBook'

export { Container, Content, Card, Title, Form, InfoBook }
