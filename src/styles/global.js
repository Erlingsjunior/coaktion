import { createGlobalStyle, css } from 'styled-components/macro'

export default createGlobalStyle`

  * {
		margin: 0;
		padding: 0;
		box-sizing: border-box;
  }

	${({ theme }) => css`
    html {
      /* a cada 1rem será considerado 10px */
      font-size: 62.5%;
    }

    body,
    #root {
      font-size: ${theme.font.sizes.small};
      font-family: ${theme.font.family};
      color: ${theme.colors.blue};
      background: ${theme.colors.grayLight};
      height: 100vh;
    }

    a {
      text-decoration: none;
    }

    .custom-scroll {
      overflow: auto;
      &::-webkit-scrollbar-track {
        border-radius: 6px;
        background-color: #afafaf4f;
      }
      &::-webkit-scrollbar {
        width: 6px;
        height: 6px;
      }
      &::-webkit-scrollbar-thumb {
        border-radius: 6px;
        background-color: #bfbcb7;
      }
    }
  `}

 `
