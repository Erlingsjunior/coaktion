import { createTheme } from '@material-ui/core/styles'
import Theme from './theme'

const ThemeMUI = createTheme({
  palette: {
    primary: {
      main: Theme.colors.blue
    }
  },
  typography: {
    fontFamily: Theme.font.family,
    fontSize: 14,
    display4: {
      fontSize: 14
    },
    display3: {
      fontSize: 14
    },
    display2: {
      fontSize: 14
    },
    display1: {
      fontSize: 14
    },
    headline: {
      fontSize: 14
    },
    title: {
      fontSize: 14
    },
    subheading: {
      fontSize: 14
    },
    body2: {
      fontSize: 14
    },
    body1: {
      fontSize: 14
    },
    caption: {
      fontSize: 14
    },
    button: {
      fontSize: 14
    }
  }
})

export default ThemeMUI
