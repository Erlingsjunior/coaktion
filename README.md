# Co.Aktion - Front

## Tecnologias

Tecnologias usadas:

- [Create React App](https://create-react-app.dev/)
- [Styled Components](https://styled-components.com/)
- [Material UI](https://material-ui.com/)

## Rodar localmente

Clone o projeto

```bash
  git clone https://bitbucket.org/Erlingsjunior/coaktion/src/main/
```

Entre na pasta

```bash
  cd coaktion
```

Instale as dependências

```bash
  yarn install
```

Inicie o server

```bash
  yarn start
```

Abra http://localhost:3000 em seu navegador.

## Rodar remotamente

O projeto está hospedado na plataforma [Netlify](https://www.netlify.com/):

https://clever-jepsen-da82be.netlify.app/

## Páginas

- Tela inicial que carrega todos os livros
  cadastrados no banco.
- Tela de detalhes para ver mais sobre o livro
- Login, acessando o endereço: https://clever-jepsen-da82be.netlify.app/login
- Telas de admin: tela de listagem, tela de edição, tela de detalhes do livro.

## Login

Para conseguir logar, digite o login e senha abaixo:

- login: admin@admin.com
- password: admin

OBS: O login é expirado a cada 5 minutos.